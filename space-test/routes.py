from handler.views import index, projects, tests, collections, container, tools
from handler.api.tools import new_tool, api


# TODO: переделать ссылку на контейнеры container-{cont_id}
def setup_routes(app):
    routes = [
        ('GET', '/', index, 'main'),
        ('GET', '/collections', collections, 'collections'),
        ('GET', '/tests', tests, 'tests'),
        ('GET', '/projects', projects, 'projects'),
        ('GET', '/tools', tools, 'tools'),

        ('GET', '/container-{cont_id}', container, 'container'),
        ('GET', '/api', api, 'api'),
        ('POST', '/api/tool', new_tool, 'tool')
    ]

    for route in routes:
        app.router.add_route(route[0], route[1], route[2], name=route[3])



"""
    routes = [
        ('GET', '/', ChatList, 'main'),
        ('GET', '/ws', WebSocket, 'chat'),
        ('*', '/login', Login, 'login'),
        ('*', '/signin', SignIn, 'signin'),
        ('*', '/signout', SignOut, 'signout'),
    ]
"""
