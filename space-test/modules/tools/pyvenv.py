from dotmap import DotMap
import venv
import sys
import subprocess
import os.path as path


class BuilderUtilsMixin(object):
    def run_in_venv(self, context, command, args=None, shell=False):
        """
        Запустите команду в venv
        :param context: объект environment
        :param command: команда выполняемая в venv
        :param args: аргументы которые передаются в команду
        :param shell:  запуск в командной строке
        :return: в случае успешного выполнения возвращает True, else False
        """
        venv_command_path = path.join(context.bin_path, command)
        if path.exists(venv_command_path):
            command = venv_command_path
        call_args = [command]
        if args is not None:
            call_args.extend(args)
        try:
            return subprocess.call(call_args, shell=shell) == 0
        except (subprocess.SubprocessError, OSError):
            return False

    def install_pip_requirements(self, context, requirements_path):
        """
        Установка requirements в виртуальную среду
        :param context: объект environment
        :param requirements_path: путь до requirements файла
        """
        self.run_in_venv(context, 'pip', ['install', '-r', requirements_path])


class Environment(venv.EnvBuilder, BuilderUtilsMixin):
    def __init__(self, config, path):
        """
        :param config: параметры виртуальной среды
        :param path: путь до дтректории скрипта
        """
        self.path = path
        super().__init__(config.returnList)

    def create(self):
        """
        Создает виртуальную python среду.
        """
        env_dir = self.path + '\\venv\\'
        context = self.ensure_directories(env_dir)
        print(context)
        self.create_configuration(context)
        self.setup_python(context)
        self.install_scripts(context, self.path + '\\test.py')
        self.post_setup(context)




class ConfigEnv:
    def __init__(self):
        self.system_site_packages = False
        self.clear = True
        self.symlinks = False
        self.upgrade = False
        self.with_pip = False
        self.prompt = None

    def openConfig(self, config):
        config = DotMap(config)
        self.system_site_packages = config.system_site_packages
        self.clear = config.clear
        self.symlinks = config.symlinks
        self.upgrade = config.upgrade
        self.with_pip = config.with_pip
        self.prompt = config.prompt

    def returnList(self):
        return self.system_site_packages, self.clear, self.symlinks, self.upgrade, self.with_pip, self.prompt


a = ConfigEnv()
scripts_path = 'C:\\Tim\\eltex\\space\\tools\\Scripts\\common\\test'

b = Environment(a, scripts_path)
b.create()