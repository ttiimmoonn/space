import pathlib
import json
import logging


BASE_DIR = pathlib.Path(__file__).parent.parent
config_path = BASE_DIR / 'config' / 'space-conf.json'
templates_path = BASE_DIR / 'space-test' / 'front' / 'templates'
static_path = BASE_DIR / 'space-test' / 'front' / 'static'
containers_path = BASE_DIR / 'containers'
log_path = BASE_DIR / 'log'


def create_logger(path):
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    handler = logging.FileHandler("{}/space_error.log".format(path), "w", encoding=None, delay="true")
    handler.setLevel(logging.ERROR)
    formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    handler = logging.FileHandler("{}/space_all.log".format(path), "w")
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


def get_config(path):
    with open(path) as f:
        config = json.load(f)
    config['templates_path'] = templates_path
    config['static_path'] = static_path
    config['containers_path'] = containers_path
    return config


logger = create_logger(log_path)
config = get_config(config_path)
