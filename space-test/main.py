from aiohttp import web
from dotmap import DotMap
import jinja2
import asyncpg
import asyncio


from routes import setup_routes
from settings import config, logger

import aiohttp_jinja2


async def init_app():
    """Initialize the application server."""
    try:
        application = web.Application()
        application['config'] = DotMap(config)

        application['pool'] = await asyncpg.create_pool(
            ** application['config'].database
        )

        aiohttp_jinja2.setup(
            application, loader=jinja2.FileSystemLoader(str(application['config'].templates_path)))

        application.router.add_static(
            '/static/',
            path=str(application['config'].static_path),
            name='static'
        )

        setup_routes(application)
        return application

    except Exception as e:
        logger.error('Error create application.')
        logger.error(e)
        return False

if __name__ == '__main__':
    try:
        loop = asyncio.get_event_loop()
        logger.info('Init app...')
        app = loop.run_until_complete(init_app())
        if not app:
            raise IOError("Errors run unit init app.")
        else:
            web.run_app(app)
    except Exception as ex:
        logger.error('Error run application.')
        logger.error(ex)
