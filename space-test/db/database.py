import asyncio
import asyncpg
import json

# TODO: дописать ввод/вывод данных в db
async def table_entry(app):
    pool = app['pool']
    async with pool.acquire() as connection:
        async with connection.transaction():
            result = await connection.fetchval()
