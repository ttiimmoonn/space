from aiohttp import web
import random
import json
from dotmap import DotMap


async def api(request):
    print(request.rel_url)
    status = random.choice(['active', 'errors'])
    conteiner = {'id': '4002141', 'status': status}
    return web.Response(text=json.dumps(conteiner))


async def new_tool(request):
    '''
    method:
        post
    url:
        /api/tool?name=<name-tool>
    сontent:
        {"tags_project": [], "description": ""}
    description:
        Создание нового инструмента
    '''
    try:
        tool = request.query['name']
        print("Creating new tools with name: ", tool)

        res = await request.text()
        res = DotMap(json.loads(res))

        res.name = tool
        print("Tools ", res)
        print("Tools tags", res.tags_project)
        print("Tools description", res.description)

        response_obj = {'status': 'success'}
        return web.Response(text=json.dumps(response_obj), status=200)
    except Exception as e:
        response_obj = {'status': 'failed', 'reason': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)
