from aiohttp import web
import aiohttp_jinja2

project = {'title': 'Space-test', 'proj_list': [
        {'proj_name': 'Base Test', 'type': 'ssw-3.12', 'status': 'active', 'id': '22jd9129j'},
        {'proj_name': 'Auto Test', 'type': 'ssw-3.11', 'status': 'errors', 'id': '23jdd129j'}
    ]}

@aiohttp_jinja2.template('index.html')
async def index(request):
    return project


@aiohttp_jinja2.template('collections.html')
async def collections(request):
    return project


@aiohttp_jinja2.template('projects.html')
async def projects(request):
    return project


@aiohttp_jinja2.template('tests.html')
async def tests(request):
    return project


@aiohttp_jinja2.template('container.html')
async def container(request):
    return project


@aiohttp_jinja2.template('tools.html')
async def tools(request):
    return project
